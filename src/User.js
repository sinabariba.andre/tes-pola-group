import React, { useState, useEffect } from "react";
import styled from "styled-components";

const User = ({ className }) => {

  const api =fetch('http://api.quotable.io/random?tags=history|civil-rights').then(dt=>dt)
  console.log(api)
  const getLocalData = () => {
    let user = localStorage.getItem("users");
    if (user) {
      return JSON.parse(localStorage.getItem("users"));
    } else {
      return user;
    }
  };
  const clearState = () => {
    setName("");
    setEmail("");
    setPhone("");
    setGender("");
  };

  const [items, setItems] = useState([getLocalData()]);
  const [toggleSubmit, setToggleSubmit] = useState(true);
  const [isEditUser, setIsEditUser] = useState(null);
  const deleteUser = (id) => {
    const updateItem = items.filter((elem, index) => {
      return index !== id;
    });
    setItems(updateItem);
  };

  const editUser = (id) => {
    let newEditUser = items.find((el) => {
      return el.id === id;
    });

    setToggleSubmit(false);
    setName(newEditUser.name);
    setEmail(newEditUser.email);
    setPhone(newEditUser.phone);
    setGender(newEditUser.gender);
    setIsEditUser(id);
  };

  useEffect(() => {
    localStorage.setItem("users", JSON.stringify(items));
  }, [items]);

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [gender, setGender] = useState("");

  const onSubmitForm = async (e) => {
    e.preventDefault();

    // Input validation alert :

    if (!name || !email || !phone || !gender) {
      alert("All input must be filled !");
    } else if (name && email && phone && gender && !toggleSubmit) {
      setItems(
        items.map((el) => {
          if (el.id === isEditUser) {
            setToggleSubmit(true);
            clearState();
            return { ...el, name, email, phone, gender };
          }
          return el;
        })
      );
    } else {
      items.map((el) => {
        // Email validation alert :
        if (email === el.email) {
          alert("Email is already used !");
          setEmail("");
        } else {
          const id = new Date().getTime().toString();
          setItems([...items, { id, name, email, phone, gender }]);
          clearState();
          document.getElementById("form").reset();
        }
      });
    }
  };

  const onResetForm = (e) => {
    e.preventDefault();
  };

  return (
    <>
      <div className={className}>
        <h1>Users</h1>
        <form id="form" onReset={(e) => onResetForm}>
          <div className="form-group">
            <label>Name :</label>
            <input
              aria-label="name-input"
              type="text"
              onChange={(e) => setName(e.target.value)}
              value={name}
              placeholder="User Example"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <label>Email :</label>
            <input
              aria-label="email-input"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              placeholder="username@example.com"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <label>Phone :</label>
            <input
              aria-label="phone-input"
              type="number"
              onChange={(e) => setPhone(e.target.value)}
              placeholder="081123123123"
              value={phone}
              pattern="^(\+62|62|0)8[1-9][0-9]{6,9}$"
              className="form-control"
            />
          </div>
          <p>Gender : </p>
          <select
            aria-label="gender-input"
            className="form-control"
            onChange={(e) => setGender(e.target.value)}
          >
            <option value="">Pick a gender</option>
            <option value="male">Male</option>
            <option value="female">Female</option>
          </select>
          <div className="button-group">
            <button className="btn btn-secondary" type="reset">
              Clear
            </button>
            {toggleSubmit ? (
              <button
                onClick={onSubmitForm}
                className="btn btn-secondary"
                type="submit"
              >
                Create
              </button>
            ) : (
              <button
                onClick={onSubmitForm}
                className="btn btn-secondary"
                type="submit"
              >
                Update
              </button>
            )}
          </div>
        </form>
        <br />
        <table aria-label="table-output">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Gender</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {items
              ? items.map((dt, i) => (
                  <tr key={i}>
                    <td>{i + 1}</td>
                    <td>{dt.name}</td>
                    <td>{dt.email}</td>
                    <td>{dt.phone}</td>
                    <td>{dt.gender}</td>
                    <td className="action-buttons">
                      <button
                        onClick={() => editUser(dt.id)}
                        className="btn btn-warning" style={{backgroundColor:'yellow'}}
                      >
                        Edit
                      </button>
                      <button
                        className="btn btn-danger btn-delete"
                        style={{backgroundColor:'red'}}
                        onClick={(e) => deleteUser(i)}
                      >
                        Delete
                      </button>
                    </td>
                  </tr>
                ))
              : ""}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default styled(User)`
  margin: 100px auto;
  width: 470px;

  input,
  select {
    margin-bottom: 10px;
    display: block;
    width: 100%;
  }

  table,
  th,
  td {
    border: 1px solid black;
  }

  table {
    width: calc(100%);
    border-collapse: collapse;
  }

  td:not(:first-child) {
    max-width: 100px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }

  p {
    margin-bottom: 2px;
  }

  .button-group {
    display: flex;
    justify-content: space-between;
    margin-top: 25px;
    button {
      width: 100px;
      height: 35px;
      cursor: pointer;
    }
  }

  select:invalid {
    color: #666;
  }
  select.grey {
    color: grey;
  }
  option[value=""][disabled] {
    color: grey;
  }
  option {
    color: #000;
  }

  td.action-buttons {
    text-align: center;
    padding: 3px 0px;
    max-width: initial;
    button {
      cursor: pointer;
      margin: 0px 3px;
    }
  }
`;
